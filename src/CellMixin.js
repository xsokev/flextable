export default {
  computed: {
    _cellClass: function(){
      let _class = [];
      if(this.header){
        _class.push("th");
        if(this.orderable){
          _class.push("th-ordered");
          if(this.sort==='asc'){
            _class.push('ordered-asc');
          } else if (this.sort==='desc') {
            _class.push('ordered-desc');
          } else {
            _class.push('ordered');
          }
        }
      } else {
        _class.push("td");
      }
      _class.push(this.cellClass);
      return _class.join(' ');
    },
    _cellStyle: function() {
      let align = 'flex-start';
      let valign = '';
      if(this.align === 'center'){
        align = 'center';
      } else if(this.align === 'right'){
        align = 'flex-end';
      }
      if(this.valign === 'top'){
        valign = 'flex-start';
      } else if(this.valign === 'middle'){
        valign = 'center';
      } else if(this.valign === 'bottom'){
        valign = 'flex-end';
      }
      return {
        flex: (this.grow ? '1':'0')+' 0 '+this.width+'px',
        justifyContent: align,
        alignItems: valign
      }
    }
  },
  methods: {
    renderContent: function(){
      if(this.header){
        return this.title;
      } else {
        if(this.type === 'normal'){
          if(typeof this.render === "function"){
            const content = this.render.call(this, this.data, this._self);
            if(typeof content === "string"){
              return content
            } else if(content instanceof HTMLElement) {
              this.$refs.cell && this.$refs.cell.firstChild && this.$refs.cell.removeChild(this.$refs.cell.firstChild);
              this.$nextTick(() => {
                this.$refs.cell.appendChild(content);
              });
            }
          } else {
            return this.data && this.data[this.field];
          }
        }
      }
    }
  }
}