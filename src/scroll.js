export const addScrollListener = (element, handler, scope) => {
  let lastScrollPos = 0;
  let moving = false;

  const _scrollListener = (e) => {
    lastScrollPos = e.target.scrollTop;
    if (!moving) {
      window.requestAnimationFrame(function() {
        handler.call(scope, lastScrollPos);
        setTimeout(() => handler.call(scope, lastScrollPos), 10);
        moving = false;
      });
    }
    moving = true;
  };
  const _mouseEnterListener = () => { element.addEventListener('scroll', _scrollListener); };
  const _mouseLeaveListener = () => { element.removeEventListener('scroll', _scrollListener); };

  element.addEventListener('mouseenter', _mouseEnterListener);
  element.addEventListener('mouseleave', _mouseLeaveListener);

  return {
    removeScrollListener: () => {
      element.removeEventListener('mouseenter', _mouseEnterListener);
      element.removeEventListener('mouseleave', _mouseLeaveListener);
      element.removeEventListener('scroll', _scrollListener);
    }
  }
};