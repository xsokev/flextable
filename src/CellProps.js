export default {
  props: {
    cellClass: {type: String, default: ''},
    field: String,
    visible: {type: Boolean, default: true},
    header: {type: Boolean, default: false},
    title: String,
    orderable: Boolean,
    sort: String,
    width: {type: Number, default: 100},
    grow: Boolean,
    pinned: Boolean,
    align: {type: String, default: 'left'},
    valign: {type: String, default: 'center'},
    type: {type: String, default: 'normal'},
    context: Object,
    data: Object,
    render: Function
  }
}